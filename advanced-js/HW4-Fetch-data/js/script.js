// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript
//  AJAX (АЯКС - Asynchronous JavaScript And XML) - це технологія яка дозволяє
// відправляти та отримувати дані з сервер без перезавантаження веб - сторінки,
// що, відповідно, зменшить кількість запитів до сервера, а також об’єм підвантажуваних даних.
// В основному технологія використовується для підвантаження окремих даних, відправки 
// даних форм, а саме авторизація, добавлення коментарів чи відправки повідомлень.

fetch("https://ajax.test-danit.com/api/swapi/films")
  .then((response) => response.json())
  .then((films) => {
    const fetchCharacters = (characterUrls) => {
      const characterPromises = characterUrls.map((characterUrl) =>
        fetch(characterUrl).then((response) => response.json())
      );
      return Promise.all(characterPromises);
    };

    const filmsContainer = document.getElementById("films");
    films.forEach((film) => {
      try {
        const filmElement = document.createElement("div");

        filmElement.innerHTML =
          "<h2>Episode " +
          film.episodeId +
          ": " +
          film.name +
          "</h2><p>" +
          film.openingCrawl +
          "</p>";
        filmsContainer.appendChild(filmElement);

        const charactersList = document.createElement("ul");
        filmElement.appendChild(charactersList);

        fetchCharacters(film.characters)
          .then((characters) => {
            characters.forEach((character) => {
              const characterItem = document.createElement("li");
              characterItem.textContent = character.name;
              charactersList.appendChild(characterItem);
            });
          })
          .catch((error) => {
            console.error("An error occurred:", error);
          });
      } catch (error) {
        console.error("An error occurred:", error);
      }
    });
  });

