// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript
//  Асинхронність у JavaScript означає здатність виконувати операції, які займають
// багато часу або чекають на завершення зовнішніх дій, без блокування виконання
// інших частин коду.Вміння працювати асинхронно дозволяє зробити програми більш
// ефективними та здатними до обробки багатьох операцій одночасно. У традиційному
// синхронному коді, операції виконуються послідовно: одна операція повинна завершитися,
// перш ніж розпочнеться наступна. Асинхронний код у JavaScript дозволяє виконувати
// такі довгі або блокуючі операції без затримки виконання іншого коду.Замість того,
// щоб чекати на завершення операції, він реєструє зворотний виклик(callback)
// або використовує об'єкт Promise для продовження виконання інших частин коду.

async function findLocation() {
  try {
    const ipResponse = await fetch("http://api.ipify.org/?format=json");
    const ipData = await ipResponse.json();
    const ipAddress = ipData.ip;

    const locationResponse = await fetch(
      `http://ip-api.com/json/${ipAddress}?fields=status,message,continent,country,countryCode,region,regionName,city,district,zip,lat,lon,timezone,isp,org,as,query`
    );
    const locationData = await locationResponse.json();

    const result = document.getElementById("result");
    result.innerHTML = `
                    <p>Континент: ${locationData.continent}</p>
                    <p>Країна: ${locationData.country}</p>
                    <p>Регіон: ${locationData.regionName}</p>
                    <p>Місто: ${locationData.city}</p>
                    <p>Район: ${locationData.district}</p>
                `;
  } catch (error) {
    console.log("Сталася помилка:", error);
  }
}
