// 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
//  Концепція прототипів, яка використовується в JavaScript, полягає в слідуючому.Якщо об'єкт B є прототипом
// об'єкта A, то всякий раз, коли у B є властивість, наприклад колір, A  успадкує той же самий
// колір, якщо інше не вказано явно.І нам не потрібно повторювати всю інформацію про A,
// яку він успадковує від  B.
//
// 2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
//  Ключове слово super можна використовувати методом constructor для виклику конструктора батьківського класу.

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }

  get age() {
    return this._age;
  }

  get salary() {
    return this._salary;
  }

  set name(name) {
    this._name = name;
  }

  set age(age) {
    this._age = age;
  }

  set salary(salary) {
    this._salary = salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }

  get salary() {
    return this._salary * 3;
  }

  get lang() {
    return this._lang;
  }
}

const programmer1 = new Programmer("Alex", 34, 3500, ["Python", "JavaScript"]);
const programmer2 = new Programmer("Piter", 35, 5000, ["Java", "C++"]);

console.log("Programmer 1:");
console.log("Name:", programmer1.name);
console.log("Age:", programmer1.age);
console.log("Salary:", programmer1.salary);
console.log("Languages:", programmer1.lang);
console.log();

console.log("Programmer 2:");
console.log("Name:", programmer2.name);
console.log("Age:", programmer2.age);
console.log("Salary:", programmer2.salary);
console.log("Languages:", programmer2.lang);
