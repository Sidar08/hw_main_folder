class Card {
  constructor(id, title, text, userName, userEmail) {
    this.id = id;
    this.title = title;
    this.text = text;
    this.userName = userName;
    this.userEmail = userEmail;
  }

  createCardElement() {
    const card = document.createElement("div");
    card.classList.add("card");

    const titleElement = document.createElement("h2");
    titleElement.classList.add("title");
    titleElement.textContent = this.title;

    const textElement = document.createElement("p");
    textElement.classList.add("text");
    textElement.textContent = this.text;

    const userElement = document.createElement("p");
    userElement.classList.add("user");
    userElement.textContent = `${this.userName} (${this.userEmail})`;

    const deleteButton = document.createElement("button");
    deleteButton.classList.add("delete-button");
    deleteButton.textContent = "Delete";
    deleteButton.addEventListener("click", this.deleteCard.bind(this));

    card.appendChild(titleElement);
    card.appendChild(textElement);
    card.appendChild(userElement);
    card.appendChild(deleteButton);

    return card;
  }

  deleteCard() {
    fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
      method: "DELETE",
    })
      .then((response) => {
        if (response.ok) {
          const cardElement = document.getElementById(`card-${this.id}`);
          if (cardElement) {
            cardElement.remove();
          }
        } else {
          console.error("Error deleting card");
        }
      })
      .catch((error) => {
        console.error("Error deleting card:", error);
      });
  }
}

fetch("https://ajax.test-danit.com/api/json/users")
  .then((response) => response.json())
  .then((users) => {
    fetch("https://ajax.test-danit.com/api/json/posts")
      .then((response) => response.json())
      .then((posts) => {
        displayTwitterFeed(users, posts);
      });
  })
  .catch((error) => {
    console.error("Error fetching data:", error);
  });

function displayTwitterFeed(users, posts) {
  const twitterFeed = document.getElementById("twitter-feed");

  posts.forEach((post) => {
    const user = users.find((user) => user.id === post.userId);
    if (user) {
      const card = new Card(
        post.id,
        post.title,
        post.body,
        user.name,
        user.email
      );
      const cardElement = card.createCardElement();
      cardElement.id = `card-${post.id}`;
      twitterFeed.appendChild(cardElement);
    }
  });
}
