/*
  1. Опишіть, як можна створити новий HTML тег на сторінці.
  Створити html елемент можна за допомогою метода document.createElement
Метод призначений для створення тегів або, інакше кажучи html-елементів.
  2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра
  insertAdjacentHTML() розбирає вказаний текст як HTML і вставляє отримані вузли (nodes) у 
DOM дерево у вказану позицію. Ця функція не переписує наявні елементи, що запобігає додатковій 
серіалізації та тому працює швидше, ніж маніпуляції з innerHTML.
  targetElement.insertAdjacentHTML(position, text);
  Параметри:
  - position
DOMString - визначає позицію елемента, що додається щодо елемента, що викликав метод. Повинно відповідати 
одному з наступних значень (чутливо до регістру):

'beforebegin': до самого element (до відкриває тега).
'afterbegin': відразу після тега element, що відкриває (перед першим нащадком).
'beforeend': відразу перед закриваючим тегом element (після останнього нащадка).
'afterend': після element (після закриває тега).

- text
Рядок, який буде проаналізовано як HTML або XML і вставлено в DOM дерево документа.
  3. Як можна видалити елемент зі сторінки?
  Метод Element.remove() видаляє елемент із DOM-дерева, в якому він знаходиться.

*/

function createList(array, parent = document.body) {
  
  let listElement = document.createElement("ul");
  let listItem = document.createElement("li");

  
  parent.appendChild(listElement);

  for (let i = 0; i < array.length; ++i) {
    listItem.textContent = array[i];
    listElement.appendChild(listItem);
    listItem = document.createElement("li");
  }
}

createList(["1", "2", "3", "sea", "user", 23]);
createList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);
