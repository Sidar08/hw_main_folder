/*
  1. Чому для роботи з input не рекомендується використовувати клавіатуру?
  Події клавіатури слід використовувати, коли ми хочемо обробляти дії клавіатури (віртуальна клавіатура 
також враховується). Наприклад, щоб реагувати на клавіші зі стрілками Up та Down або гарячі 
клавіші (включаючи комбінації клавіш).
  Події повязані з клавіатурою вішаються на всю сторінку, а не на окремий елемент. 
Якщо event клавіатури використовувати для input,він не буде працювати взагалі, або буде працювати не коректно

*/

const btn = document.querySelectorAll(`.btn`);

const keyFunction = {
  Enter: function (event) {
    btn[0].style.backgroundColor = "blue";
  },
  s: function (event) {
    btn[1].style.backgroundColor = "blue";
  },
  e: function (event) {
    btn[2].style.backgroundColor = "blue";
  },
  o: function (event) {
    btn[3].style.backgroundColor = "blue";
  },
  n: function (event) {
    btn[4].style.backgroundColor = "blue";
  },
  l: function (event) {
    btn[5].style.backgroundColor = "blue";
  },
  z: function (event) {
    btn[6].style.backgroundColor = "blue";
  },
};

document.addEventListener(`keydown`, function (event) {
  btn.forEach(function (element) {
    if ((element.style.backgroundColor = "blue")) {
      element.style.backgroundColor = "black";
    }
  });
  keyFunction[event.key](event);
});
