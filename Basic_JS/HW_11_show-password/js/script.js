const form = document.querySelector(`.password-form`);
const eye = document.querySelectorAll(`.icon-password`);
const input = document.querySelectorAll(`input`);

form.addEventListener(`click`, (event) => {
  if (event.target.id === `eye1`) {
    if (event.target.classList.contains(`fa-eye`)) {
      event.target.classList.replace(`fa-eye`, `fa-eye-slash`);
      event.target.previousElementSibling.setAttribute(`type`, `text`);
    } else if (event.target.classList.contains(`fa-eye-slash`)) {
      event.target.classList.replace(`fa-eye-slash`, `fa-eye`);
      event.target.previousElementSibling.setAttribute(`type`, `password`);
    }
  } else if (event.target.id === `eye2`) {
    if (event.target.classList.contains(`fa-eye`)) {
      event.target.classList.replace(`fa-eye`, `fa-eye-slash`);
      event.target.previousElementSibling.setAttribute(`type`, `text`);
    } else if (event.target.classList.contains(`fa-eye-slash`)) {
      event.target.classList.replace(`fa-eye-slash`, `fa-eye`);
      event.target.previousElementSibling.setAttribute(`type`, `password`);
    }
  }
});


let btn = document.querySelector(`.btn`);

let paragraf = document.createElement(`p`);
btn.before(paragraf);

btn.addEventListener(`click`, function (event) {
  event.preventDefault();
  if (password1.value !== password2.value) {
    paragraf.innerText = `Потрібно ввести однакові значення`;
    paragraf.style.color = `red`;
    return paragraf;
  } else {
    alert(`You are welcome`);
  }
  document.querySelector("#myform").reset();
});
