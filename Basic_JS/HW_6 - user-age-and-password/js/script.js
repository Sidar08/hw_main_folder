/**
 * 1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
 *     екранування символів - це заміна в тексті керуючих символів на відповідні текстові підстановки. 
 * Потрібно просто додати перед лапками всередині рядка тексту символ \ (обернений слеш). Цей спосіб 
 * дозволяє додати символи, які б в мові програмування вважалися б не коректними
 * 2. Які засоби оголошення функцій ви знаєте?
 *      Існують такі засоби оголошення функціі:
 *  - Оголошення функції (Function declaration)
 *      Оголошення функції складається з ключового слова function, за яким слідує обов'язкове 
 *    ім'я функції, списку параметрів у круглих дужках (param1, ..., paramN) та фігурних дужок 
 *    {...}, що обмежують основний код.
 *  - Функціональний вираз (Function expression)
 *     Вираз функції визначається ключовим словом function, за яким слідує необов'язкове 
 *    ім'я функції, списком параметрів у круглих дужках (param1, ..., paramN) і парою фігурних дужок 
 *    {...}, що обмежують тіло код.
 *  - Стрілка функція
 *    Стрілка функція визначається за допомогою пари круглих дужок, які містять список параметрів 
 *   (param1, ..., paramN), за яким слідує стрілка => і пара фігурних дужок {...}, що обмежують тіло функції.
 *  - Функція-генератор (Generator function)
 *      Функція-генератор JavaScript повертає об'єкт Generator. Його синтаксис аналогічний виразу функції, 
 *    оголошення функції або оголошення методу, тільки йому потрібен символ зірочки *.
 *  - Використання new Function 
 * 3. Що таке hoisting, як він працює для змінних та функцій?
 *    Hoisting, тобто спливання, підняття, це механізм, при якому змінні та оголошення функції 
 *  піднімаються вгору по своїй області видимості перед виконанням коду. Однією з переваг 
 *  підйому є те, що він дозволяє нам використовувати функції перед їх оголошенням у коді.
 */

const name1 = prompt(`Enter your First Name`);
const name2 = prompt(`Enter your Last Name`);
const birthday = prompt(`Enter your birsday date(in format dd.mm.yyyy)?`);
let splitString = birthday.split(".");
let reverseArray = splitString.reverse();
let joinArray = reverseArray.join(".");

function CreateNewUser(firstName, lastName) {
  this.firstName = firstName,
    this.lastName = lastName,
    this.getLogin =  function () {
      return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    };
  this.birthday = birthday,
    this.getAge = function () {
      let today = new Date();
      const birth = new Date(joinArray);
    let age = Number(today.getFullYear() - birth.getFullYear());
    let birthNow = new Date(
      today.getFullYear(),
      birth.getMonth(),
      birth.getDate()
    );
    if (today < birthNow) {
      age = age - 1;
    }
      return age;
    };
  this.getPassword = function () {
    const birth = new Date(joinArray);
    let password =
      this.firstName[0].toUpperCase() +
      this.lastName.toLowerCase() +
      birth.getFullYear();
    return password
      
  };
}

let newUser = new CreateNewUser(name1, name2);

console.log(newUser);
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());

