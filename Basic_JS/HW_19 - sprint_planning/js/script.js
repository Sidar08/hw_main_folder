
const today = new Date();

function deadline(array1, array2, data) {
  let sumArray1 = array1.reduce((total, value) => {
    return total + value;
  }, 0);
  let sumArray2 = array2.reduce((total, value) => {
    return total + value;
  }, 0);
  let pointInHour = sumArray1 / 8;
  if (sumArray1 < sumArray2 && today !== data) {
    let extraTime = (sumArray2 - sumArray1) / pointInHour;
    alert(
      `Команді розробників доведеться витратити додатково ${extraTime} годин після дедлайну, щоб виконати всі завдання в беклозі`
    );
  } else {
    let timeForWork = sumArray1 / pointInHour / 8;
    alert(
      `Усі завдання будуть успішно виконані за ${timeForWork} днів до настання дедлайну!`
    );
  }
}

console.log(deadline([2, 5, 3], [1, 5, 8, 3], 23 / 03 / 2023));


