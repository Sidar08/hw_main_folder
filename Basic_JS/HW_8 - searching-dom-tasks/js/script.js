/*
  1. Опишіть своїми словами що таке Document Object Model (DOM)
      DOM – це деревоподібне представлення сайту, що завантажується в браузер, 
    у вигляді серії об'єктів, вкладених один в одного. Воно визначає логічний 
    каркас документа, способи доступу до нього та управління ним. З його 
    допомогою ми можемо створювати документи, переміщатися за їхньою структурою, 
    а також додавати, змінювати або видаляти його елементи та їх вміст.
  2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
    Якщо всередині innerText будуть ще якісь елементи HTML зі своїм вмістом, 
  він проігнорує самі елементи і поверне їх внутрішній текст. innerHTML - 
  покаже текстову інформацію рівно по одному елементу.
  3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
    Для звернення до будь-якого елементу сторінки в DOM інтерфейсі введені спеціальні
  пошукові методи. Пошук може здійснюється за різними параметрами елементів, 
  тому за допомогою них можна знайти будь-який елемент.
    getElementById ()
  Об'єкт document має вбудований метод getElementById (). Він виконує пошук елемента 
  за його унікальним ідентифікатором id.
    getElementsByName ()
  Об'єкт document має вбудований метод getElementsByName (). Пошук ведеться по всіх 
  елементах сторінки із заданим атрибутом name. 
    getElementsByTagName ()
  Для будь-якого елементу сторінки доступний метод getElementsByTagName (). Даний метод виконує пошук по тегу.
    getElementsByClassName ()
  Для будь-якого елементу сторінки доступний метод getElementsByClassName (). Даний метод виконує пошук по глобальному атрибуту class.
    querySelectorAll ()
  Для будь-якого елементу сторінки доступний метод querySelectorAll (). Метод повертає список всіх елементів, 
  що задовольняють заданій CSS-селектору. При його виклику пошук ведеться всередині даного елемента.
    querySelector ()
  Для будь-якого елементу сторінки доступний метод querySelector (). Метод повертає перший елемент, 
  що задовольняє заданому CSS-селектору. При його виклику пошук ведеться всередині даного елемента.
    На мою думку, кращим способом являється - querySelector (), тому що він є самим універсальним методом. 
  Основна перевага даного методу щодо querySelectorAll () в тому, що він зупиняється при першому 
  знайденому збігу. Це дозволяє збільшити продуктивність. Наприклад, якщо наперед відомо, 
  що результат буде складатися з одного елемента, то краще скористатися даним методом.
*/


let paragraf = document.getElementsByTagName(`p`);

for (let i = 0; i < paragraf.length; i++) {
  paragraf[i].style.backgroundColor = "#ff0000";
}

let getElementById = document.getElementById("optionsList");

console.log(getElementById);

console.log(getElementById.parentElement);
console.log(getElementById.childNodes);
console.log(getElementById.childNodes[0].nodeName);
console.log(getElementById.childNodes[0].nodeType);

const elem = document.getElementById("testParagraph");
elem.textContent = "This is a paragraph";

let elements = document.querySelector(".main-header").children;
console.log(elements);

[...document.querySelector(".main-header").children].forEach((i) =>
  i.classList.add("nav-item")
);

let getElementsByClassName = document.getElementsByClassName("section-title");

console.log(getElementsByClassName);

for (let i = 0; i < getElementsByClassName.length; i++) {
  getElementsByClassName[i].classList.remove("section-title");
}
