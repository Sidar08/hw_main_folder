/**
 * 1. Описати своїми словами навіщо потрібні функції у програмуванні
 *    Функції в прогамуванні потрібні з декількох причин:
 *      - спрощує текст програми. Якщо текст програми розбитий на функції, кот робиться більш читабельним;
 *      - зменшує дублювання коду;
 * 2. Описати своїми словами, навіщо у функцію передавати аргумент.
 *    - arguments - це локальна змінна, доступна всередині будь-якої (нестрілочної) функції.
 *    Об'єкт arguments дозволяє посилатися аргументу функції всередині неї. Він складається із
 *    переданих у функцію аргументів, дефолтне значення "0"
 * 3. Що таке оператор return та як він працює всередині функції?
 *      Оператор return використовується у функціях повернення даних після виконання роботи функції.
 *    Якщо функція повинна обробити якісь дані та потім повернути їх, то для повернення даних необхідний оператор return
 */

let num1 = Number(prompt("Enter your firstNumber!"));

while (Number.isNaN(num1)) {
  num1 = Number(prompt("Enter your firstNumber!"));
}

let num2 = Number(prompt("Enter your secondNumber!"));

while (Number.isNaN(num2)) {
  num2 = Number(prompt("Enter your secondNumber!"));
}

let operation = prompt("Enter operation");
while (
  operation !== "+" &&
  operation !== "-" &&
  operation !== "/" &&
  operation !== "*"
) {
  operation = prompt("Enter operation");
}

let count = function () {
  switch (operation) {
    case "+":
      return num1 + num2;
    case "-":
      return num1 - num2;
    case "/":
      return num1 / num2;
    case "*":
      return num1 * num2;
    default: 
      alert("This operation does not exist");
  }
}();
console.log(count);
