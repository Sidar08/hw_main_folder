/**
 * 1. Опишіть своїми словами, що таке метод об'єкту
 *      Метод - це властивість об'єкта, яка в той же час є функцією.
 *    Методи визначаються так само, як і звичайні функції,
 *    за винятком, що вони присвоюються властивості об'єкта.
 * 2. Який тип даних може мати значення властивості об'єкта?
 *      Значення властивості об'єкта може мати такі типи даних:
 *    - number;
 *    - string;
 *    - boolean;
 *    - array;
 *    - underfined.
 * 3. Об'єкт це посилальний тип даних. Що означає це поняття?
 *      Посилальний тип – це “тип специфікації”. Ми не можемо явно
 *    використовувати його, але він використовується всередині мови.
 *    Значення посилального типу – це комбінація трьох значення (base, name, strict), де:
 *    - base – це об’єкт.
 *    - name – це назва властивості.
 *    - strict – це true якщо діє use strict.
 */

const name1 = prompt(`Enter your First Name`);
const name2 = prompt(`Enter your Last Name`);

function CreateNewUser(firstName, lastName) {
  this.firstName = firstName,
  this.lastName = lastName,
  this.getLogin = function () {
    return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
  };
}

let newUser = new CreateNewUser(name1, name2);

console.log(newUser.getLogin());
