const navigationIcon = document.querySelector(".material-symbols-outlined");
const navigationList = document.querySelector(".navigation__list");
const elementVisibility = document.querySelector(".element-visibility");
const InstagramShotText1 = document.querySelector(".InstagramShot-text1");
const images = document.querySelectorAll(".images");
const lastImage = document.querySelector(".lastImage");

navigationIcon.addEventListener("click", function () {
  if (navigationIcon.innerText === "menu") {
    navigationIcon.innerText = "close";
  } else {
    navigationIcon.innerText = "menu";
  }
  navigationList.classList.toggle("invisible");
});

const mediaQuery = window.matchMedia("(min-width: 768px)");
if (mediaQuery.matches == true) {
  navigationList.classList.remove("invisible");
  elementVisibility.classList.remove("element-visibility");
  InstagramShotText1.classList.remove("element-visibility");
  images.forEach((n) => n.classList.remove("element-visibility"));
}

if (window.matchMedia("(max-width: 768px)").matches == true) {
  navigationList.classList.add("invisible");
  
}

if (window.matchMedia("(min-width: 1200px)").matches == true) {
  lastImage.classList.remove("element-visibility");
}


