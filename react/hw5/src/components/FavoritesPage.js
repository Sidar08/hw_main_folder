import React, { useState, useEffect } from "react";
import FavoriteProductCard from "./FavoriteProductCard";
import { renderFavoritesIcon } from "./HeaderIcons";
import { useSelector, useDispatch } from "react-redux";
import { removeFromFavorites } from "../redux/actions";

const FavoritesPage = () => {

  const favorites = useSelector((state) => state.favorites); // Отримуємо обрані товари зі стору

  const dispatch = useDispatch();

  // Функція для видалення товару з обраного через Redux
  const handleRemoveFromFavorites = (product) => {
    dispatch(removeFromFavorites(product));
  };

  return (
    <div className="favorites-page">
      <h2>Обране</h2>
      <div className="product-list">
        {favorites.map((product) => (
          <FavoriteProductCard
            key={product.id}
            product={product}
            removeFromFavorites={handleRemoveFromFavorites}
            isFavorite={true} // Передаємо прапорець, що товар є вибраним
          />
        ))}
      </div>
    </div>
  );
};

export default FavoritesPage;
