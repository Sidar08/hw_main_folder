import React from "react";
import ProductCard from "./ProductCard";

const Home = ({ products, addToCart, addToFavorites, removeFromFavorites }) => {
  return (
    <div className="product-list">
      {products.map((product) => (
        <ProductCard
          key={product.id}
          product={product}
          addToCart={addToCart}
          addToFavorites={addToFavorites}
          removeFromFavorites={removeFromFavorites}
        />
      ))}
    </div>
  );
};

export default Home;
