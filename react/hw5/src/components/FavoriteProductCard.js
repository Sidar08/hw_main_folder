import React from "react";

const FavoriteProductCard = ({ product, removeFromFavorites, isFavorite }) => {
  // Функція для видалення товару з Вибраного
  const handleRemoveFromFavorites = () => {
    removeFromFavorites(product);
  };

  return (
    <div key={product.id} className="favorite-product-card">
      <img src={product.image} alt={product.name} />
      <h3>{product.name}</h3>
      <p>Ціна: ${product.price}</p>

      {/* Кнопка для видалення з Вибраного */}
      {isFavorite && (
        <button onClick={handleRemoveFromFavorites}>&#9733;</button>
      )}
    </div>
  );
};

export default FavoriteProductCard;
