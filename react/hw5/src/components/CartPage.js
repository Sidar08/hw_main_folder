import React, { useState } from "react";
import Modal from "./Modal";
import { useDispatch } from "react-redux";
import { ConnectedCartPage } from "./ConnectedCartPage";
import { useSelector } from "react-redux";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { setCart } from "../redux/actions";
import { removeFromCartAction, clearCart } from "../redux/actions";

const CheckoutSchema = Yup.object().shape({
  firstName: Yup.string().required("Це поле обов'язкове"),
  lastName: Yup.string().required("Це поле обов'язкове"),
  age: Yup.number()
    .typeError("Введіть коректний вік")
    .required("Це поле обов'язкове"),
  address: Yup.string().required("Це поле обов'язкове"),
  phone: Yup.string()
    .matches(/^\d+$/, "Введіть коректний номер телефону")
    .required("Це поле обов'язкове"),
});

const CartPage = ({ removeFromCart, cart }) => {
  const [selectedProductIndex, setSelectedProductIndex] = useState(null);

  const openDeleteConfirmationModal = (productIndex) => {
    setSelectedProductIndex(productIndex);
  };

  const closeDeleteConfirmationModal = () => {
    setSelectedProductIndex(null);
  };
  const dispatch = useDispatch();

  const handleDeleteProduct = () => {
    if (selectedProductIndex !== null) {
      dispatch(removeFromCartAction(selectedProductIndex)); // Відправка дії до Redux store
      setSelectedProductIndex(null);
    }
  };

  return (
    <div className="CartPage">
      {cart.map((product, index) => (
        <div key={index}>
          <div className="cart-item">
            <img src={product.image} alt={product.name} />
            <div>
              <p className="item-name">{product.name}</p>
              <p className="item-price">${product.price}</p>
            </div>
            <span
              className="delete-icon"
              onClick={() => openDeleteConfirmationModal(index)}
            >
              &#10006;
            </span>
          </div>
        </div>
      ))}
      {selectedProductIndex !== null && (
        <Modal
          header="Підтвердження видалення"
          closeButton={closeDeleteConfirmationModal}
          text={`Ви дійсно хочете видалити "${cart[selectedProductIndex].name}" з кошика?`}
          actions={
            <>
              <button
                className="modal-button"
                onClick={closeDeleteConfirmationModal}
              >
                Скасувати
              </button>
              <button className="modal-button" onClick={handleDeleteProduct}>
                Видалити
              </button>
            </>
          }
        />
      )}
      <Formik
        initialValues={{
          firstName: "",
          lastName: "",
          age: "",
          address: "",
          phone: "",
        }}
        validationSchema={CheckoutSchema}
        onSubmit={(values) => {
          console.log("Інформація для покупки:", values); // Виведення інформації із форми
          dispatch(clearCart()); // Очищення кошика
          localStorage.removeItem("cart");
          console.log("Придбані товари:", cart); // Виведення придбаних товарів
        }}
      >
        {({ errors, touched }) => (
          <Form>
            <div className="form-group">
              <Field
                type="text"
                name="firstName"
                className={`form-control ${
                  touched.firstName && errors.firstName ? "is-invalid" : ""
                }`}
                placeholder="Ім'я"
              />
              <ErrorMessage
                name="firstName"
                component="div"
                className="invalid-feedback"
              />
            </div>
            <div className="form-group">
              <Field
                type="text"
                name="lastName"
                className={`form-control ${
                  touched.lastName && errors.lastName ? "is-invalid" : ""
                }`}
                placeholder="Прізвище"
              />
              <ErrorMessage
                name="lastName"
                component="div"
                className="invalid-feedback"
              />
            </div>
            <div className="form-group">
              <Field
                type="number"
                name="age"
                className={`form-control ${
                  touched.age && errors.age ? "is-invalid" : ""
                }`}
                placeholder="Вік"
              />
              <ErrorMessage
                name="age"
                component="div"
                className="invalid-feedback"
              />
            </div>
            <div className="form-group">
              <Field
                type="text"
                name="address"
                className={`form-control ${
                  touched.address && errors.address ? "is-invalid" : ""
                }`}
                placeholder="Адреса доставки"
              />
              <ErrorMessage
                name="address"
                component="div"
                className="invalid-feedback"
              />
            </div>
            <div className="form-group">
              <Field
                type="text"
                name="phone"
                className={`form-control ${
                  touched.phone && errors.phone ? "is-invalid" : ""
                }`}
                placeholder="Мобільний телефон"
              />
              <ErrorMessage
                name="phone"
                component="div"
                className="invalid-feedback"
              />
            </div>
            <button type="submit" className="btn btn-primary">
              Checkout
            </button>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default CartPage;
