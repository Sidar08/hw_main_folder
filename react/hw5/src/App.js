import React from "react";
import "./scss/App.scss";
import AppRoutes from "../src/components/AppRoutes";

const App = () => {
  return <AppRoutes />;
};

export default App;
