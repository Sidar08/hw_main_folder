import React from "react";
import "./Modal.scss";

const Modal = ({ header, closeButton, text, actions }) => {
  const closeModal = () => {
    // Call the provided close function to close the modal
    if (typeof closeButton === "function") {
      closeButton();
    }
  };

  const stopPropagation = (event) => {
    // This function prevents the click event from propagating to parent elements
    event.stopPropagation();
  };

  return (
    <div className="modal-overlay" onClick={closeModal}>
      <div className="modal" onClick={stopPropagation}>
        {closeButton && (
          <button className="close-button" onClick={closeModal}>
            &times;
          </button>
        )}

        <h2>{header}</h2>
        <div className="modal-content">{text}</div>
        <div className="modal-actions">{actions}</div>
      </div>
    </div>
  );
};

export default Modal;
