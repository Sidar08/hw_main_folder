import React, { useState } from "react";
import "./App.scss";
import Button from "./Button";
import Modal from "./Modal";

const App = () => {
  const [showFirstModal, setShowFirstModal] = useState(false);
  const [showSecondModal, setShowSecondModal] = useState(false);

  const openFirstModal = () => {
    setShowFirstModal(true);
  };

  const closeFirstModal = () => {
    setShowFirstModal(false);
  };

  const openSecondModal = () => {
    setShowSecondModal(true);
  };

  const closeSecondModal = () => {
    setShowSecondModal(false);
  };

  return (
    <div className="app">
      <div className="button-container">
        <Button
          backgroundColor="blue"
          text="Open first modal"
          onClick={openFirstModal}
        />
        <Button
          backgroundColor="green"
          text="Open second modal"
          onClick={openSecondModal}
        />
      </div>

      {showFirstModal && (
        <Modal
          header="First Modal"
          closeButton={closeFirstModal}
          text="This is the text for the first modal."
          actions={
            <>
              <Button
                backgroundColor="red"
                text="Cancel"
                onClick={closeFirstModal}
              />
              <Button
                backgroundColor="green"
                text="Save"
                onClick={closeFirstModal}
              />
            </>
          }
        />
      )}

      {showSecondModal && (
        <Modal
          header="Second Modal"
          closeButton={closeSecondModal}
          text="This is the text for the second modal."
          actions={
            <>
              <Button
                backgroundColor="orange"
                text="Close"
                onClick={closeSecondModal}
              />
            </>
          }
        />
      )}
    </div>
  );
};

export default App;
