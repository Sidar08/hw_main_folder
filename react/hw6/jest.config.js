module.exports = {
  testEnvironment: "jsdom", // Використовувати середовище для тестування браузера
  setupFilesAfterEnv: ["@testing-library/jest-dom/extend-expect"], // Підключення @testing-library/jest-dom
  moduleNameMapper: {
    "\\.(css|less|sass|scss)$": "identity-obj-proxy",
  },
  transform: {
    "^.+\\.(js|jsx)$": "babel-jest",
  },
};
