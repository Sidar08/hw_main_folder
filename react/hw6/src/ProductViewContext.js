import React, { createContext, useState, useContext } from "react";

const ProductViewContext = createContext();

export const useProductView = () => {
  return useContext(ProductViewContext);
};

export const ProductViewProvider = ({ children }) => {
  const [view, setView] = useState("card"); // Початковий вид відображення - картки

  const toggleView = () => {
    setView((prevView) => (prevView === "list" ? "card" : "list"));
  };

  return (
    <ProductViewContext.Provider value={{ view, toggleView }}>
      {children}
    </ProductViewContext.Provider>
  );
};
