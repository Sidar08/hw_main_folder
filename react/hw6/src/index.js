import React from "react";
import { createRoot } from "react-dom/client"; // Змінено імпорт
import { Provider } from "react-redux";
import { store } from "./redux/store";
import "./index.css";
import AppRoutes from "./components/AppRoutes";
import { ProductViewProvider } from "./ProductViewContext";

const root = createRoot(document.getElementById("root")); // Використовуємо createRoot

root.render(
  <Provider store={store}>
    <ProductViewProvider>
      <React.StrictMode>
        <AppRoutes />
      </React.StrictMode>
    </ProductViewProvider>
  </Provider>
);