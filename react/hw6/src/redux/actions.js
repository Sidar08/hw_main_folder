import {
  SET_PRODUCTS,
  ADD_TO_CART,
  ADD_TO_FAVORITES,
  REMOVE_FROM_FAVORITES,
  REMOVE_FROM_CART,
  SET_CART,
  SET_FAVORITES,
  UPDATE_SELECTED_PRODUCT,
  UPDATE_SHOW_CART_MODAL,
} from "./actionTypes";

export const setCart = (cart) => ({
  type: SET_CART,
  payload: cart,
});

export const setFavorites = (favorites) => ({
  type: SET_FAVORITES,
  payload: favorites,
});

export const setProducts = (products) => ({
  type: SET_PRODUCTS,
  payload: products,
});


export const addToCart = (product) => ({
  type: ADD_TO_CART,
  payload: product,
});

export const addToFavorites = (product) => ({
  type: ADD_TO_FAVORITES,
  payload: product,
});

export const removeFromCartAction = (productIndex) => ({
  type: REMOVE_FROM_CART,
  payload: productIndex, // Використовувати індекс замість товару
});

export const removeFromFavorites = (product) => ({
  type: REMOVE_FROM_FAVORITES,
  payload: product,
});

export const removeFromCart = (product) => ({
  type: REMOVE_FROM_CART,
  payload: product,
});

export const updateProducts = (products) => ({
  type: "UPDATE_PRODUCTS",
  payload: products,
});

export const updateFavorites = (favorites) => ({
  type: "UPDATE_FAVORITES",
  payload: favorites,
});

export const updateSelectedProduct = (product) => ({
  type: "UPDATE_SELECTED_PRODUCT",
  payload: product,
});

export const updateShowCartModal = (showModal) => ({
  type: "UPDATE_SHOW_CART_MODAL",
  payload: showModal,
});

export const setSelectedProduct = (product) => ({
  type: "UPDATE_SELECTED_PRODUCT",
  payload: product,
});

export const setShowCartModal = (showModal) => ({
  type: "UPDATE_SHOW_CART_MODAL",
  payload: showModal,
});

export const clearCart = () => ({
  type: "CLEAR_CART",
});
