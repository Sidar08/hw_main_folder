import React, { useState } from "react";
import Button from "./Button";
import Modal from "./Modal";
import { useDispatch } from "react-redux"; // Додайте import для useDispatch
import {
  addToCart,
  addToFavorites,
  removeFromFavorites,
} from "../redux/actions"; // Додайте необхідні імпорти

const ProductCard = ({ product, addToFavorites, removeFromFavorites }) => {
  const [isFavorite, setIsFavorite] = useState(product.isFavorite);
  const [showConfirmationModal, setShowConfirmationModal] = useState(false);
  const dispatch = useDispatch(); // Отримайте функцію диспетчера

  const toggleFavorite = () => {
    if (isFavorite) {
      removeFromFavorites(product);
    } else {
      addToFavorites(product);
    }
    setIsFavorite(!isFavorite);
  };

  const openConfirmationModal = () => {
    setShowConfirmationModal(true);
  };

  const closeConfirmationModal = () => {
    setShowConfirmationModal(false);
  };

  const handleAddToCart = () => {
    dispatch(addToCart(product)); // Викликайте диспетчер addToCart для додавання товару в кошик
    closeConfirmationModal(); // Закрийте модальне вікно після додавання товару
  };

  return (
    <div key={product.id} className="product-card">
      <img src={product.image} alt={product.name} />
      <h3>{product.name}</h3>
      <p>Ціна: ${product.price}</p>

      <Button
        backgroundColor="blue"
        text="Додати в кошик"
        onClick={openConfirmationModal}
      />
      <button
        className={`favorite-button ${isFavorite ? "favorite" : ""}`}
        onClick={toggleFavorite}
      >
        &#9733;
      </button>

      {showConfirmationModal && (
        <Modal
          header="Підтвердження"
          closeButton={closeConfirmationModal}
          text={`Додати "${product.name}" в кошик?`}
          actions={
            <>
              <Button
                backgroundColor="green"
                text="Додати"
                onClick={handleAddToCart} // Викликати функцію для додавання товару в кошик
              />
              <Button
                text="Скасувати"
                backgroundColor="red"
                onClick={closeConfirmationModal}
              />
            </>
          }
        />
      )}
    </div>
  );
};

export default ProductCard;
