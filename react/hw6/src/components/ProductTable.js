import React from "react";

const ProductTable = ({ products }) => {
  return (
    <table>
      <thead>
        <tr>
          <th>ID</th>
          <th>Назва товару</th>
          <th>Ціна</th>
          <th>Дії</th>
        </tr>
      </thead>
      <tbody>
        {products.map((product) => (
          <tr key={product.id}>
            <td>{product.id}</td>
            <td>{product.name}</td>
            <td>${product.price}</td>
            <td>
              {/* Додайте кнопки або дії для кожного товару, якщо необхідно */}
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default ProductTable;
