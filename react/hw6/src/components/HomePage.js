// import React from "react";
// import ProductCard from "./ProductCard";

// const Home = ({ products, addToCart, addToFavorites, removeFromFavorites }) => {
//   return (
//     <div className="product-list">
//       {products.map((product) => (
//         <ProductCard
//           key={product.id}
//           product={product}
//           addToCart={addToCart}
//           addToFavorites={addToFavorites}
//           removeFromFavorites={removeFromFavorites}
//         />
//       ))}
//     </div>
//   );
// };

// export default Home;

// import React from "react";
import ProductCard from "./ProductCard";
import ProductTable from "./ProductTable"; // Додайте імпорт компонента таблиці
import { useProductView } from "../ProductViewContext";

const Home = ({ products, addToCart, addToFavorites, removeFromFavorites }) => {
  const { view, toggleView } = useProductView();

  return (
    <div>
      <button onClick={toggleView} className="buttonView">
        Змінити вид відображення: {view === "card" ? "Таблиця" : "Картки"}
      </button>
      <div className="product-list">
        {/* Відображайте відповідний вид товарів в залежності від вибраного варіанта */}
        {view === "card" ? (
          products.map((product) => (
            <ProductCard
              key={product.id}
              product={product}
              addToCart={addToCart}
              addToFavorites={addToFavorites}
              removeFromFavorites={removeFromFavorites}
            />
          ))
        ) : (
          <ProductTable products={products} />
        )}
      </div>
    </div>
  );
};

export default Home;
