import React from "react";
import { render, fireEvent } from "@testing-library/react";
import FavoriteProductCard from "../components/FavoriteProductCard";

describe("FavoriteProductCard", () => {
  it("should render a button when isFavorite is true", () => {
    const product = {
      id: 1,
      name: "Product 1",
      price: 10,
      image: "product1.jpg",
    };
    const { getByText } = render(
      <FavoriteProductCard
        product={product}
        removeFromFavorites={() => {}}
        isFavorite={true}
      />
    );
    const removeButton = getByText("★"); // Тут використовуйте символ, який ви відображаєте на кнопці

    expect(removeButton).toBeInTheDocument();
  });

  it("should not render a button when isFavorite is false", () => {
    const product = {
      id: 2,
      name: "Product 2",
      price: 20,
      image: "product2.jpg",
    };
    const { queryByText } = render(
      <FavoriteProductCard
        product={product}
        removeFromFavorites={() => {}}
        isFavorite={false}
      />
    );
    const removeButton = queryByText("★"); // Тут використовуйте символ, який ви відображаєте на кнопці

    expect(removeButton).toBeNull();
  });

  it("should call removeFromFavorites when the button is clicked", () => {
    const product = {
      id: 3,
      name: "Product 3",
      price: 30,
      image: "product3.jpg",
    };
    const removeFromFavoritesMock = jest.fn(); // Створюємо функцію-мок для removeFromFavorites
    const { getByText } = render(
      <FavoriteProductCard
        product={product}
        removeFromFavorites={removeFromFavoritesMock}
        isFavorite={true}
      />
    );
    const removeButton = getByText("★"); // Тут використовуйте символ, який ви відображаєте на кнопці

    fireEvent.click(removeButton); // Спричинюємо подію кліку на кнопку

    expect(removeFromFavoritesMock).toHaveBeenCalledWith(product); // Перевіряємо, чи була викликана функція removeFromFavorites з правильним аргументом
  });
});
