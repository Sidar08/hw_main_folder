import cartReducer from "../redux/reducers";
import {
  ADD_TO_CART,
  REMOVE_FROM_CART,
  CLEAR_CART,
} from "../redux/actionTypes";

describe("cartReducer", () => {
  it("should add a product to the cart on ADD_TO_CART action", () => {
    const initialState = [];
    const action = { type: ADD_TO_CART, payload: { id: 1, name: "Product 1" } };
    const newState = cartReducer({ cart: initialState }, action); // Виправлено структуру стану
    expect(newState.cart).toEqual([{ id: 1, name: "Product 1" }]); // Змінено спосіб перевірки
  });

  it("should remove a product from the cart on REMOVE_FROM_CART action", () => {
    const initialState = {
      cart: [
        { id: 1, name: "Product 1" },
        { id: 2, name: "Product 2" },
      ],
    }; // Виправлено початковий стан
    const action = { type: REMOVE_FROM_CART, payload: 0 }; // Removing the first item
    const newState = cartReducer(initialState, action);
    expect(newState.cart).toEqual([{ id: 2, name: "Product 2" }]); // Змінено спосіб перевірки
  });

  it("should clear the cart on CLEAR_CART action", () => {
    const initialState = {
      cart: [
        { id: 1, name: "Product 1" },
        { id: 2, name: "Product 2" },
      ],
    }; // Виправлено початковий стан
    const action = { type: CLEAR_CART };
    const newState = cartReducer(initialState, action);
    expect(newState.cart).toEqual([]); // Змінено спосіб перевірки
  });
});