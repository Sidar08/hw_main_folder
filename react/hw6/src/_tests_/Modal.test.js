import React from "react";
import { render, fireEvent } from "@testing-library/react";
import Modal from "../components/Modal";

test("Modal renders correctly", () => {
  const closeButtonFn = jest.fn(); // Створюємо функцію-заглушку
  const { getByText } = render(
    <Modal
      header="Test Modal"
      closeButton={closeButtonFn}
      text="This is a test modal."
      actions={<button>Test Button</button>}
    />
  );

  const modalHeader = getByText("Test Modal");
  const modalText = getByText("This is a test modal.");
  const modalButton = getByText("Test Button");

  expect(modalHeader).toBeInTheDocument();
  expect(modalText).toBeInTheDocument();
  expect(modalButton).toBeInTheDocument();

  fireEvent.click(modalButton); // Симулюємо клік на кнопку в модальному вікні
});
