import React from "react";
import { render, fireEvent } from "@testing-library/react";
import CartModal from "../components/CartModal";

describe("CartModal", () => {
  // Мок-функції для передачі як пропси
  const mockProduct = { id: 1, name: "Товар", price: 10 };
  const mockCloseCartModal = jest.fn();
  const mockAddToCart = jest.fn();

  it("renders correctly", () => {
    const { getByText } = render(
      <CartModal
        product={mockProduct}
        closeCartModal={mockCloseCartModal}
        addToCart={mockAddToCart}
      />
    );

    // Перевірка, що текст та кнопки містяться в модальному вікні
    expect(getByText("Додати до кошика")).toBeInTheDocument();
    expect(
      getByText(`Додати ${mockProduct.name} до кошика?`)
    ).toBeInTheDocument();

    // Перевірка наявності кнопок "Скасувати" і "Додати"
    expect(getByText("Скасувати")).toBeInTheDocument();
    expect(getByText("Додати")).toBeInTheDocument();
  });

  it("calls closeCartModal when Cancel button is clicked", () => {
    const { getByText } = render(
      <CartModal
        product={mockProduct}
        closeCartModal={mockCloseCartModal}
        addToCart={mockAddToCart}
      />
    );

    const cancelButton = getByText("Скасувати");

    fireEvent.click(cancelButton);

    // Перевірка, що mockCloseCartModal була викликана один раз
    expect(mockCloseCartModal).toHaveBeenCalledTimes(1);
  });

  it("calls addToCart and closeCartModal when Add button is clicked", () => {
    const { getByText } = render(
      <CartModal
        product={mockProduct}
        closeCartModal={mockCloseCartModal}
        addToCart={mockAddToCart}
      />
    );

    const addButton = getByText("Додати");

    fireEvent.click(addButton);

    // Перевірка, що mockAddToCart і mockCloseCartModal були викликані один раз кожна
    expect(mockAddToCart).toHaveBeenCalledTimes(1);
    expect(mockCloseCartModal).toHaveBeenCalledTimes(2);
  });
});
