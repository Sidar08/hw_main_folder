import productsReducer from "../redux/reducers";
import {
  SET_PRODUCTS,
  ADD_TO_FAVORITES,
  REMOVE_FROM_FAVORITES,
} from "../redux/actionTypes";

describe("productsReducer", () => {
  it("should set products with isFavorite set to false on SET_PRODUCTS action", () => {
    const initialState = {
      products: [], // Оприділіть products як ключ у вашому початковому стані
      cart: [], // Якщо він є в початковому стані
      selectedProduct: null, // Якщо він є в початковому стані
      showCartModal: false, // Якщо він є в початковому стані
      favorites: [], // Якщо він є в початковому стані
    };
    const products = [
      { id: 1, name: "Product 1" },
      { id: 2, name: "Product 2" },
    ];
    const action = { type: SET_PRODUCTS, payload: products };
    const newState = productsReducer(initialState, action);
    expect(newState.products).toEqual([
      { id: 1, name: "Product 1", isFavorite: false },
      { id: 2, name: "Product 2", isFavorite: false },
    ]);
  });

  it("should toggle isFavorite on ADD_TO_FAVORITES and REMOVE_FROM_FAVORITES actions", () => {
    const initialState = {
      products: [
        { id: 1, name: "Product 1", isFavorite: false },
        { id: 2, name: "Product 2", isFavorite: false },
      ],
      cart: [], // Якщо він є в початковому стані
      selectedProduct: null, // Якщо він є в початковому стані
      showCartModal: false, // Якщо він є в початковому стані
      favorites: [], // Якщо він є в початковому стані
    };

    const action1 = { type: ADD_TO_FAVORITES, payload: { id: 1 } };
    const newState1 = productsReducer(initialState, action1);
    expect(newState1.products[0].isFavorite).toBe(true);

    const action2 = { type: REMOVE_FROM_FAVORITES, payload: { id: 2 } };
    const newState2 = productsReducer(newState1, action2);
    expect(newState2.products[1].isFavorite).toBe(true);
  });
});
