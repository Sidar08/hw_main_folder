import favoritesReducer from "../redux/reducers";
import { ADD_TO_FAVORITES, REMOVE_FROM_FAVORITES } from "../redux/actionTypes";

describe("favoritesReducer", () => {
  it("should add a product to favorites on ADD_TO_FAVORITES action", () => {
    const initialState = { favorites: [] }; // Змінено початковий стан
    const action = {
      type: ADD_TO_FAVORITES,
      payload: { id: 1, name: "Product 1" },
    };
    const newState = favoritesReducer(initialState, action);
    expect(newState.favorites).toEqual([{ id: 1, name: "Product 1" }]);
  });

  it("should remove a product from favorites on REMOVE_FROM_FAVORITES action", () => {
    const initialState = {
      favorites: [
        { id: 1, name: "Product 1" },
        { id: 2, name: "Product 2" },
      ],
    }; // Змінено початковий стан
    const action = { type: REMOVE_FROM_FAVORITES, payload: { id: 1 } };
    const newState = favoritesReducer(initialState, action);
    expect(newState.favorites).toEqual([{ id: 2, name: "Product 2" }]);
  });
});