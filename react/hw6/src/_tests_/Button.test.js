import React from "react";
import { render, fireEvent } from "@testing-library/react";
import Button from "../components/Button";

test("Button renders correctly", () => {
  const onClick = jest.fn(); // Створюємо функцію-заглушку
  const { getByText } = render(
    <Button backgroundColor="blue" text="Click me" onClick={onClick} />
  );

  const button = getByText("Click me");
  expect(button).toBeInTheDocument();

  fireEvent.click(button); // Симулюємо клік на кнопку

  expect(onClick).toHaveBeenCalled(); // Перевіряємо, чи був викликаний обробник подій
});
