import React from "react";
import { useSelector, useDispatch } from "react-redux";
import CartPage from "./CartPage";
import { removeFromCartAction } from "../redux/actions"; // Імпортуємо дію видалення з кошика

const ConnectedCartPage = () => {
  const cart = useSelector((state) => state.cart); // Отримайте список товарів зі стану

  const dispatch = useDispatch();

  const removeFromCart = (productIndex) => {
    dispatch(removeFromCartAction(productIndex)); // Викликайте дію для видалення товару з кошика
  };
console.log("Cart from ConnectedCartPage:", cart);

  return <CartPage cart={cart} removeFromCart={removeFromCart} />;
};

export default ConnectedCartPage;
