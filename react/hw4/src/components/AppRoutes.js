import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  setCart,
  setFavorites,
  setProducts,
  addToCart,
  addToFavorites,
  setSelectedProduct,
  setShowCartModal,
  updateProducts,
  updateFavorites,
  updateSelectedProduct,
  updateShowCartModal,
  removeFromCartAction,
} from "../redux/actions";
import "../scss/App.scss";
import ProductCard from "./ProductCard";
import CartModal from "./CartModal";
import CartPage from "./CartPage";
import FavoritesPage from "./FavoritesPage";
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import Home from "./HomePage";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faShoppingCart, faHeart } from "@fortawesome/free-solid-svg-icons";
import ConnectedCartPage from "./ConnectedCartPage";

const AppRoutes = () => {
  const dispatch = useDispatch();
  const products = useSelector((state) => state.products);
  const favorites = useSelector((state) => state.favorites); // Визначте favorites
  const cart = useSelector((state) => state.cart);
  const showCartModal = useSelector((state) => state.showCartModal);
  const selectedProduct = useSelector((state) => state.selectedProduct);

  const favoriteCount = favorites ? favorites.length : 0;

  console.log("Products from state:", products);

  const initializeApp = () => {
    fetch("/products.json")
      .then((response) => response.json())
      .then((data) => {
        const storedProducts = data.map((product) => ({
          ...product,
          isFavorite: false,
        }));

        dispatch(setProducts(storedProducts));
      })
      .catch((error) =>
        console.error("Помилка при отриманні даних про товари:", error)
      );
  };

  useEffect(() => {
    initializeApp();
  }, []);

  const addToCartDispatch = (product) => {
    const updatedCart = [...cart, product];
    dispatch(setCart(updatedCart)); // Оновити стан кошика в Redux store
    console.log("Updated cart:", updatedCart);
    localStorage.setItem("cart", JSON.stringify(updatedCart)); 
    console.log("Cart data saved to localStorage:", updatedCart);
  };

  // Функція додає вибраний товар до обраних
  const addToFavoritesDispatch = (product) => {
    if (favorites) {
      const updatedProducts = products.map((p) =>
        p.id === product.id ? { ...p, isFavorite: true } : p
      );
      dispatch(addToFavorites(product));
      dispatch(setProducts(updatedProducts));
      const updatedFavorites = [...favorites, product];
      dispatch(setFavorites(updatedFavorites));
      localStorage.setItem("favorites", JSON.stringify(updatedFavorites));
    }
  };

  const removeFromFavorites = (product) => {
    const updatedFavorites = favorites.filter((fav) => fav.id !== product.id);
    dispatch(setFavorites(updatedFavorites));
    // Збереження оновлених обраних товарів в localStorage
    localStorage.setItem("favorites", JSON.stringify(updatedFavorites));
  };
  // Функція для відкриття модального вікна підтвердження додавання товару в кошик
  const openCartModal = (product) => {
    dispatch(setSelectedProduct(product));
    dispatch(setShowCartModal(true));
  };

  const closeCartModal = () => {
    dispatch(setSelectedProduct(null));
    dispatch(setShowCartModal(false));
  };

  const renderCartIcon = (cartItemCount) => {
    return (
      <div className="cart-icon">
        <FontAwesomeIcon icon={faShoppingCart} />
        <span className="cart-count">{cartItemCount}</span>
      </div>
    );
  };

  const renderFavoritesIcon = (favoriteItemCount) => {
    return (
      <div className="favorites-icon">
        <FontAwesomeIcon icon={faHeart} />
        <span className="favorites-count">{favoriteItemCount}</span>
      </div>
    );
  };
  // Функція видаляє товар з кошика
  const removeFromCartDispatch = (productIndex) => {
    const updatedCart = cart.filter((_, index) => index !== productIndex);
    dispatch(setCart(updatedCart)); // Додати даний диспетчер для оновлення стану
    localStorage.setItem("cart", JSON.stringify(updatedCart));
  };

  // Функція для оновлення стану обраних товарів на сторінці "Головна"
  const updateFavoriteStatusOnHome = (productId, isFavorite) => {
    const updatedProducts = products.map((product) =>
      product.id === productId ? { ...product, isFavorite } : product
    );
    dispatch(updateProducts(updatedProducts)); // Використовуємо диспетчер для оновлення продуктів
  };

  const storeName = "Baby-store";

  return (
    <Router>
      <div className="app">
        <div className="header">
          <h1 className="store-name">{storeName}</h1>
          <Link to="/">Головна</Link>
          <Link to="/cart">Кошик</Link>
          <Link to="/favorites">Вибране</Link>
          {renderCartIcon(cart.length)}
          {renderFavoritesIcon(favoriteCount)}
        </div>

        <Routes>
          <Route
            path="/"
            element={
              <Home
                products={products}
                addToCart={addToCartDispatch} // Використовуємо функцію з диспетчером addToCart
                addToFavorites={addToFavoritesDispatch} // Використовуємо функцію з диспетчером addToFavorites
                removeFromFavorites={removeFromFavorites}
                updateFavoriteStatusOnHome={updateFavoriteStatusOnHome} // Передаємо функцію без змін
              />
            }
          />
          <Route path="/cart" element={<ConnectedCartPage />} />
          <Route
            path="/favorites"
            element={<FavoritesPage favorites={favorites} />}
          />
        </Routes>

        {showCartModal && selectedProduct && (
          <CartModal
            product={selectedProduct}
            closeCartModal={closeCartModal}
            addToCart={addToCartDispatch}
          />
        )}
      </div>
    </Router>
  );
};

export default AppRoutes;
