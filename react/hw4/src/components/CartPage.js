import React, { useState } from "react";
import Modal from "./Modal";
import { removeFromCartAction } from "../redux/actions";
import { useDispatch } from "react-redux";
import { ConnectedCartPage } from "./ConnectedCartPage";
import { useSelector } from "react-redux";


const CartPage = ({removeFromCart, cart }) => {
  const [selectedProductIndex, setSelectedProductIndex] = useState(null);

  const openDeleteConfirmationModal = (productIndex) => {
    setSelectedProductIndex(productIndex);
  };

  const closeDeleteConfirmationModal = () => {
    setSelectedProductIndex(null);
  };
  const dispatch = useDispatch();

  const handleDeleteProduct = () => {
    if (selectedProductIndex !== null) {
      dispatch(removeFromCartAction(selectedProductIndex)); // Відправка дії до Redux store
      setSelectedProductIndex(null);
    }
  };

  return (
    <div className="CartPage">
      {cart.map((product, index) => (
        <div key={index}>
          <div className="cart-item">
            <img src={product.image} alt={product.name} />
            <div>
              <p className="item-name">{product.name}</p>
              <p className="item-price">${product.price}</p>
            </div>
            <span
              className="delete-icon"
              onClick={() => openDeleteConfirmationModal(index)}
            >
              &#10006;
            </span>
          </div>
        </div>
      ))}
      {selectedProductIndex !== null && (
        <Modal
          header="Підтвердження видалення"
          closeButton={closeDeleteConfirmationModal}
          text={`Ви дійсно хочете видалити "${cart[selectedProductIndex].name}" з кошика?`}
          actions={
            <>
              <button
                className="modal-button"
                onClick={closeDeleteConfirmationModal}
              >
                Скасувати
              </button>
              <button className="modal-button" onClick={handleDeleteProduct}>
                Видалити
              </button>
            </>
          }
        />
      )}
    </div>
  );
};

export default CartPage;
