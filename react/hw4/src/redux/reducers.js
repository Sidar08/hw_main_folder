import { combineReducers } from "redux";
import {
  SET_PRODUCTS,
  ADD_TO_CART,
  ADD_TO_FAVORITES,
  REMOVE_FROM_FAVORITES,
  REMOVE_FROM_CART,
  UPDATE_SELECTED_PRODUCT,
  UPDATE_SHOW_CART_MODAL,
} from "./actionTypes";

const productsReducer = (state = [], action) => {
  switch (action.type) {
    case SET_PRODUCTS:
      return action.payload.map((product) => ({
        ...product,
        isFavorite: false,
      }));
    case ADD_TO_FAVORITES:
    case REMOVE_FROM_FAVORITES:
      return state.map((product) =>
        product.id === action.payload.id
          ? { ...product, isFavorite: !product.isFavorite }
          : product
      );
    default:
      return state;
  }
};

const favoritesReducer = (state = [], action) => {
  switch (action.type) {
    case ADD_TO_FAVORITES:
      return [...state, action.payload];
    case REMOVE_FROM_FAVORITES:
      return state.filter((product) => product.id !== action.payload.id);
    default:
      return state;
  }
};

const cartReducer = (state = [], action) => {
  switch (action.type) {
    case ADD_TO_CART:
      const updatedCart = [...state, action.payload];
      return updatedCart;
    case REMOVE_FROM_CART:
      const filteredCart = state.filter((_, index) => index !== action.payload);
      return filteredCart;
    default:
      return state;
  }
};

const selectedProductReducer = (state = null, action) => {
  switch (action.type) {
    case UPDATE_SELECTED_PRODUCT:
      return action.payload;
    default:
      return state;
  }
};

const showCartModalReducer = (state = false, action) => {
  switch (action.type) {
    case UPDATE_SHOW_CART_MODAL:
      return action.payload;
    default:
      return state;
  }
};

const rootReducer = (state, action) => {
  const newState = combineReducers({
    products: productsReducer,
    cart: cartReducer,
    selectedProduct: selectedProductReducer,
    showCartModal: showCartModalReducer,
    favorites: favoritesReducer,
  })(state, action);

  // Перевірка на наявність змін у стані
  if (state !== newState) {
    localStorage.setItem("reduxState", JSON.stringify(newState));
  }

  return newState;
};

export default rootReducer;
