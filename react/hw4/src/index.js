import React from "react";
import { createRoot } from "react-dom"; // Змінено імпорт
import { Provider } from "react-redux";
import { store } from "./redux/store";
import "./index.css";
import AppRoutes from "./components/AppRoutes";

const root = createRoot(document.getElementById("root")); // Використовуємо createRoot

root.render(
  <Provider store={store}>
    <React.StrictMode>
      <AppRoutes />
    </React.StrictMode>
  </Provider>
);