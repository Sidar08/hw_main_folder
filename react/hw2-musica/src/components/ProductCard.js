import React from "react";
import Button from "./Button";
import "../scss/Modal.scss";

const ProductCard = ({
  product,
  addToCart,
  addToFavorites,
  removeFromFavorites,
}) => {
  return (
    <div key={product.id} className="product-card">
      <img src={product.image} alt={product.name} />
      <h3>{product.name}</h3>
      <p>Ціна: ${product.price}</p>
      
      <Button
        backgroundColor="blue"
        text="Додати в кошик"
        onClick={() => addToCart(product)}
      />
      
      <button
        className={`favorite-button ${product.isFavorite ? "favorite" : ""}`}
        onClick={() =>
          product.isFavorite
            ? removeFromFavorites(product)
            : addToFavorites(product)
        }
      >
        &#9733;
      </button>
    </div>
  );
};

export default ProductCard;
