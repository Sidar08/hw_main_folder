import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faShoppingCart, faHeart } from "@fortawesome/free-solid-svg-icons";
import "./scss/App.scss";
import ProductCard from "./components/ProductCard";
import CartModal from "./components/CartModal";

const App = () => {
  // Створюємо стани для товарів, кошика та обраних товарів
  const [products, setProducts] = useState([]);
  const [cart, setCart] = useState([]);
  const [favorites, setFavorites] = useState([]);
  const [showCartModal, setShowCartModal] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState(null);

  // Завантаження даних про товари з JSON-файлу
  useEffect(() => {
    fetch("/products.json")
      .then((response) => response.json())
      .then((data) =>
        setProducts(data.map((product) => ({ ...product, isFavorite: false })))
      )
      .catch((error) =>
        console.error("Помилка при отриманні даних про товари:", error)
      );
  }, []);

  // Функція додає вибраний товар до кошика
  const addToCart = (product) => {
    setCart([...cart, product]);
    // Збереження кошика в localStorage
    localStorage.setItem("cart", JSON.stringify([...cart, product]));
  };

  // Функція додає вибраний товар до обраних
  const addToFavorites = (product) => {
    const updatedProducts = products.map((p) =>
      p.id === product.id ? { ...p, isFavorite: true } : p
    );
    setProducts(updatedProducts);
    setFavorites([...favorites, product]);
    // Збереження обраних товарів в localStorage
    localStorage.setItem("favorites", JSON.stringify([...favorites, product]));
  };

  // Функція видаляє товар із обраних
  const removeFromFavorites = (product) => {
    const updatedProducts = products.map((p) =>
      p.id === product.id ? { ...p, isFavorite: false } : p
    );
    setProducts(updatedProducts);
    const updatedFavorites = favorites.filter((fav) => fav.id !== product.id);
    setFavorites(updatedFavorites);
    // Збереження оновлених обраних товарів в localStorage
    localStorage.setItem("favorites", JSON.stringify(updatedFavorites));
  };

  // Функція для відкриття модального вікна підтвердження додавання товару в кошик
  const openCartModal = (product) => {
    setSelectedProduct(product);
    setShowCartModal(true);
  };

  // Функція для закриття модального вікна підтвердження додавання товару в кошик
  const closeCartModal = () => {
    setSelectedProduct(null);
    setShowCartModal(false);
  };

  // При першому рендері компонента отримуємо дані кошика та обраних товарів з localStorage
  useEffect(() => {
    const storedCart = JSON.parse(localStorage.getItem("cart")) || [];
    const storedFavorites = JSON.parse(localStorage.getItem("favorites")) || [];
    setCart(storedCart);
    setFavorites(storedFavorites);
  }, []);

  const renderCartIcon = () => {
    return (
      <div className="cart-icon">
        <FontAwesomeIcon icon={faShoppingCart} />
        <span className="cart-count">{cart.length}</span>
      </div>
    );
  };

  const renderFavoritesIcon = () => {
    return (
      <div className="favorites-icon">
        <FontAwesomeIcon icon={faHeart} />
        <span className="favorites-count">{favorites.length}</span>
      </div>
    );
  };

  const storeName = "Baby-store";

  return (
    <div className="app">
      <div className="header">
        <h1 className="store-name">{storeName}</h1>
        {renderCartIcon()}
        {renderFavoritesIcon()}
      </div>

      {/* Виводимо список товарів */}
      <div className="product-list">
        {products.map((product) => (
          <ProductCard
            key={product.id}
            product={product}
            addToCart={openCartModal}
            addToFavorites={addToFavorites}
            removeFromFavorites={removeFromFavorites}
          />
        ))}
      </div>

      {/* Модальне вікно для підтвердження додавання товару в кошик */}
      {showCartModal && selectedProduct && (
        <CartModal
          product={selectedProduct}
          closeCartModal={closeCartModal}
          addToCart={addToCart}
        />
      )}
    </div>
  );
};

export default App;
