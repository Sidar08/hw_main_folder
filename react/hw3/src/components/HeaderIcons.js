import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faShoppingCart, faHeart } from "@fortawesome/free-solid-svg-icons";

const renderCartIcon = (cartItemCount) => {
  return (
    <div className="cart-icon">
      <FontAwesomeIcon icon={faShoppingCart} />
      <span className="cart-count">{cartItemCount}</span>
    </div>
  );
};

const renderFavoritesIcon = (favoriteItemCount) => {
  return (
    <div className="favorites-icon">
      <FontAwesomeIcon icon={faHeart} />
      <span className="favorites-count">{favoriteItemCount}</span>
    </div>
  );
};

export { renderCartIcon, renderFavoritesIcon };
