import React from "react";
import Modal from "./Modal";
import Button from "./Button";

const CartModal = ({ product, closeCartModal, addToCart }) => {
  return (
    <Modal
      header="Додати до кошика"
      closeButton={closeCartModal}
      text={`Додати ${product.name} до кошика?`}
      actions={
        <>
          <Button
            backgroundColor="red"
            text="Скасувати"
            onClick={closeCartModal}
          />
          <Button
            backgroundColor="green"
            text="Додати"
            onClick={() => {
              addToCart(product);
              closeCartModal();
            }}
          />
        </>
      }
    />
  );
};

export default CartModal;
