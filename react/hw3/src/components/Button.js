import React from "react";
import "../scss/Button.scss";

const Button = ({ backgroundColor, text, onClick }) => {
  return (
    <button className={`button ${backgroundColor}`} onClick={onClick}>
      {text}
    </button>
  );
};

export default Button;
