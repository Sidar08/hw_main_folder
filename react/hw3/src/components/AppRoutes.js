import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faShoppingCart, faHeart } from "@fortawesome/free-solid-svg-icons";
import "../scss/App.scss";
import ProductCard from "./ProductCard";
import CartModal from "./CartModal";
import CartPage from "./CartPage";
import FavoritesPage from "./FavoritesPage";
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import Home from "./HomePage";
import FavoriteProductCard from "./FavoriteProductCard";
import { renderCartIcon, renderFavoritesIcon } from "./HeaderIcons";

const AppRoutes = () => {
  // Створюємо стани для товарів, кошика та обраних товарів
  const [products, setProducts] = useState([]);
  const [cart, setCart] = useState([]);
  const [favorites, setFavorites] = useState([]);
  const [showCartModal, setShowCartModal] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState(null);

  const favoriteCount = favorites.length;

  // Завантаження даних про товари з JSON-файлу
  useEffect(() => {
    fetch("/products.json")
      .then((response) => response.json())
      .then((data) =>
        setProducts(data.map((product) => ({ ...product, isFavorite: false })))
      )
      .catch((error) =>
        console.error("Помилка при отриманні даних про товари:", error)
      );
  }, []);

  // Функція додає вибраний товар до кошика
  const addToCart = (product) => {
    setCart([...cart, product]);
    const updatedCart = [...cart, product];
    localStorage.setItem("cart", JSON.stringify(updatedCart));
  };

  // Функція додає вибраний товар до обраних
  const addToFavorites = (product) => {
    const updatedProducts = products.map((p) =>
      p.id === product.id ? { ...p, isFavorite: true } : p
    );
    setProducts(updatedProducts);
    setFavorites([...favorites, product]);

    const updatedFavorites = [...favorites, product];
    localStorage.setItem("favorites", JSON.stringify(updatedFavorites));
  };

  const removeFromFavorites = (product) => {
    const updatedFavorites = favorites.filter((fav) => fav.id !== product.id);
    setFavorites(updatedFavorites);
    // Збереження оновлених обраних товарів в localStorage
    localStorage.setItem("favorites", JSON.stringify(updatedFavorites));
  };
  // Функція для відкриття модального вікна підтвердження додавання товару в кошик
  const openCartModal = (product) => {
    setSelectedProduct(product);
    setShowCartModal(true);
  };

  // Функція для закриття модального вікна підтвердження додавання товару в кошик
  const closeCartModal = () => {
    setSelectedProduct(null);
    setShowCartModal(false);
  };

  useEffect(() => {
    fetch("/products.json")
      .then((response) => response.json())
      .then((data) => {
        const storedFavorites =
          JSON.parse(localStorage.getItem("favorites")) || [];
        const storedCart = JSON.parse(localStorage.getItem("cart")) || [];

        setProducts(
          data.map((product) => ({
            ...product,
            isFavorite: storedFavorites.some((fav) => fav.id === product.id),
          }))
        );
        setFavorites(storedFavorites);
        setCart(storedCart);
      })
      .catch((error) =>
        console.error("Помилка при отриманні даних про товари:", error)
      );
  }, []);

  const renderCartIcon = (cartItemCount) => {
    return (
      <div className="cart-icon">
        <FontAwesomeIcon icon={faShoppingCart} />
        <span className="cart-count">{cartItemCount}</span>
      </div>
    );
  };

  const renderFavoritesIcon = (favoriteItemCount) => {
    return (
      <div className="favorites-icon">
        <FontAwesomeIcon icon={faHeart} />
        <span className="favorites-count">{favoriteItemCount}</span>
      </div>
    );
  };
  // Функція видаляє товар з кошика
  const removeFromCart = (product) => {
    const updatedCart = cart.filter((item) => item.id !== product.id);
    setCart(updatedCart);
    // Збереження оновленого кошика в localStorage
    localStorage.setItem("cart", JSON.stringify(updatedCart));
  };

  // Функція для оновлення стану обраних товарів на сторінці "Головна"
  const updateFavoriteStatusOnHome = (productId, isFavorite) => {
    const updatedProducts = products.map((product) =>
      product.id === productId ? { ...product, isFavorite } : product
    );
    setProducts(updatedProducts);
  };

  const storeName = "Baby-store";

  return (
    <Router>
      <div className="app">
        <div className="header">
          <h1 className="store-name">{storeName}</h1>
          <Link to="/">Головна</Link>
          <Link to="/cart">Кошик</Link>
          <Link to="/favorites">Вибране</Link>
          {renderCartIcon(cart.length)}
          {renderFavoritesIcon(favoriteCount)}
        </div>

        <Routes>
          {/* Використовуємо Home для відображення списку товарів на головній сторінці */}
          <Route
            path="/"
            element={
              <Home
                products={products}
                addToCart={addToCart}
                addToFavorites={addToFavorites}
                removeFromFavorites={removeFromFavorites}
              />
            }
          />
          <Route
            path="/cart"
            element={<CartPage cart={cart} removeFromCart={removeFromCart} />}
          />
          {/* Додаємо роут для сторінки Кошик */}
          <Route
            path="/favorites"
            element={
              <FavoritesPage
                favorites={favorites}
                removeFromFavorites={removeFromFavorites}
              />
            }
          />
          {/* Додаємо роут для сторінки Вибране */}
        </Routes>

        {showCartModal && selectedProduct && (
          <CartModal
            product={selectedProduct}
            closeCartModal={closeCartModal}
            addToCart={addToCart}
          />
        )}
      </div>
    </Router>
  );
};

export default AppRoutes;