import React, { useState } from "react";
import ProductCard from "./ProductCard";
import Modal from "./Modal";

const CartPage = ({ cart, removeFromCart }) => {
  const [selectedProduct, setSelectedProduct] = useState(null);

  const openDeleteConfirmationModal = (product) => {
    setSelectedProduct(product);
  };

  const closeDeleteConfirmationModal = () => {
    setSelectedProduct(null);
  };

  const handleDeleteProduct = () => {
    if (selectedProduct) {
      removeFromCart(selectedProduct);
      closeDeleteConfirmationModal();
    }
  };

  return (
    <div className="CartPage">
      {cart.map((product) => (
        <div key={product.id}>
          <div className="cart-item">
            <img src={product.image} alt={product.name} />
            <div>
              <p className="item-name">{product.name}</p>
              <p className="item-price">${product.price}</p>
            </div>
            <span
              className="delete-icon"
              onClick={() => openDeleteConfirmationModal(product)}
            >
              &#10006;
            </span>
          </div>
        </div>
      ))}

      {/* Модальне вікно для підтвердження видалення товару з кошика */}
      {selectedProduct && (
        <Modal
          header="Підтвердження видалення"
          closeButton={closeDeleteConfirmationModal}
          text={`Ви дійсно хочете видалити "${selectedProduct.name}" з кошика?`}
          actions={
            <>
              <button onClick={closeDeleteConfirmationModal}>Скасувати</button>
              <button onClick={handleDeleteProduct}>Видалити</button>
            </>
          }
        />
      )}
    </div>
  );
};

export default CartPage;
