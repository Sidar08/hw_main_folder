import React, { useState } from "react";
import Button from "./Button";
import Modal from "./Modal";
import "../scss/Modal.scss";

const ProductCard = ({
  product,
  addToCart,
  addToFavorites,
  removeFromFavorites,
}) => {
  const [isFavorite, setIsFavorite] = useState(product.isFavorite);
  const [showConfirmationModal, setShowConfirmationModal] = useState(false);

  const toggleFavorite = () => {
    if (isFavorite) {
      removeFromFavorites(product);
    } else {
      addToFavorites(product);
    }
    setIsFavorite(!isFavorite);
  };

  const openConfirmationModal = () => {
    setShowConfirmationModal(true);
  };

  const closeConfirmationModal = () => {
    setShowConfirmationModal(false);
  };

  return (
    <div key={product.id} className="product-card">
      <img src={product.image} alt={product.name} />
      <h3>{product.name}</h3>
      <p>Ціна: ${product.price}</p>

      <Button
        backgroundColor="blue"
        text="Додати в кошик"
        onClick={openConfirmationModal}
      />
      <button
        className={`favorite-button ${isFavorite ? "favorite" : ""}`}
        onClick={toggleFavorite}
      >
        &#9733;
      </button>

      {showConfirmationModal && (
        <Modal
          header="Підтвердження"
          closeButton={closeConfirmationModal}
          text={`Додати "${product.name}" в кошик?`}
          actions={
            <>
              <Button
                backgroundColor="green"
                text="Додати"
                onClick={() => {
                  addToCart(product);
                  closeConfirmationModal();
                }}
              />
              <Button
                text="Скасувати"
                backgroundColor="red"
                onClick={closeConfirmationModal}
              >
                Скасувати
              </Button>
            </>
          }
        />
      )}
    </div>
  );
};

export default ProductCard;
