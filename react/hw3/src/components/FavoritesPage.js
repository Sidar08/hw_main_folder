import React, { useState, useEffect } from "react";
import ProductCard from "./ProductCard";
import FavoriteProductCard from "./FavoriteProductCard";
import { renderFavoritesIcon } from "./HeaderIcons";

const FavoritesPage = () => {
  const [favorites, setFavorites] = useState([]);
  const [favoriteItemCount, setFavoriteItemCount] = useState(0);

  // Завантаження обраних товарів з localStorage при першому рендері сторінки
  useEffect(() => {
    const storedFavorites = JSON.parse(localStorage.getItem("favorites")) || [];
    setFavorites(storedFavorites);
  }, []);

  // Функція для видалення товару з обраного
  const removeFromFavorites = (product) => {
    const updatedFavorites = favorites.filter((fav) => fav.id !== product.id);
    setFavorites(updatedFavorites);
    setFavoriteItemCount(updatedFavorites.length);

    localStorage.setItem("favorites", JSON.stringify(updatedFavorites));
  };

  return (
    <div>
      <h1>Сторінка "Вибране"</h1>
      {/* Виводимо список обраних товарів */}
      {favorites.map((product) => (
        <FavoriteProductCard
          key={product.id}
          product={product}
          removeFromFavorites={removeFromFavorites}
          isFavorite={true} // Оновлено: передаємо статус обраного
        />
      ))}
    </div>
  );
};

export default FavoritesPage;